#!/bin/sh

die() { echo "$*" 1>&2 ; exit 1; }

usage() { die "$0 [mingw]<release|debug>/<gcc|clang>" ; }

buildroot="/work/"

config=$1
[ ! -z "${config}" ] || die "Empty config"
echo "Building in '${config}'"

configroot="${buildroot}/build/${config}/"

mkdir -p "${buildroot}/" || die "Could not create buildroot"

# Half-arse a clone of the provided .git directory.
#
# The standard mechanism for cloning of `git clone --local` won't work
# without requiring us to use non-local clones to initalise the submodules.
# This is problematic mostly because of the heavyweight KHR submodules.
#
# Instead we copy the provided live .git directory and reset everything back
# into existence.
cp -ra /mnt/git "${buildroot}/.git" || die "Unable to copy gitdir"
cd "${buildroot}" && git checkout && git reset --hard HEAD || die "Unable to checkout root"
git submodule update --init --recursive || die "Unable to checkout submodules"
git submodule foreach git checkout master || die "Unable to checkout master for submodules"

# Create a build directory and configure the project
mkdir -p "${configroot}"|| die "Unable to create builddir"
cd "${configroot}" || die "Unable to switch to builddir"
sh -c "$(${buildroot}/build/init.py)" || die "Unable to configure project"

# Build the required targets
ninja || die "Unable to build binaries"
ninja doc || die "Unable to build docs"
ninja test || die "Tests failed"
ninja package || die "Unable to build package"

cp "${configroot}/"edict-*.tar.* /mnt/export || die "Could not copy installers"

exit 0
