#!/usr/bin/env python3

import sys
import os
import itertools


OPTIONS = {
    # compiler
    'gcc':   {'vars': {'CMAKE_CXX_COMPILER': 'g++',     'CMAKE_C_COMPILER': 'gcc'}},
    'clang': {'vars': {'CMAKE_CXX_COMPILER': 'clang++', 'CMAKE_C_COMPILER': 'clang'}},

    # profile
    'debug':   {'vars': {'CMAKE_BUILD_TYPE': 'Debug',   'LTO': 'OFF'}, 'args': ['-G', '"Unix Makefiles"']},
    'release': {'vars': {'CMAKE_BUILD_TYPE': 'Release', 'LTO': 'ON'}},

    # sanitizer
    'sanitizer': {'vars': {'SANITIZER': 'ON'}, 'require': {'PROFILE': 'release'}},

    # wrappers
    'iwyu': { 'vars': { 'CMAKE_CXX_INCLUDE_WHAT_YOU_USE': 'include-what-you-use' } },
    'tidy': { 'vars': { 'CMAKE_CXX_CLANG_TIDY': ';'.join(['clang-tidy', '--quiet', '-checks=bugprone-*,clang-analyzer-*,performance-*,portability-*,readability-,', '-warnings-as-errors=' ]) } },

    # build tools
    'make': { 'args': [ '-G', '"Unix Makefiles"' ] },
    'ninja': { 'args': [ '-G', 'Ninja' ] },

    # platforms
    'mingw': { 'vars': { 'CMAKE_TOOLCHAIN_FILE': '/home/danny/src/cmakecruft/toolchain/mingw.cmake' }, },

    # Default parameters
    '': {
        'vars': {
            "CMAKE_EXPORT_COMPILE_COMMANDS": "ON",
            "TESTS": "ON",
        },

        'args': ['-G', 'Ninja'],
    },
}


def split_all_path(path):
    parts = []

    while len(path) > 1:
        head,tail = os.path.split(path)
        yield tail
        path = head


if __name__ == '__main__':
    self_path = os.path.realpath(__file__)
    self_dir = os.path.dirname(self_path)

    build_dir = os.path.realpath(os.getcwd())

    source_dir = os.path.dirname(self_dir)

    accumulated = []

    components = ['']
    components += sys.argv[1:]
    components += itertools.takewhile(lambda x: x in OPTIONS, split_all_path(os.getcwd()))

    for key in components:
        obj = OPTIONS[key]

        vars = obj.get('vars', {})
        args = obj.get('args', [])

        accumulated += [f'"-D{key}={val}"' for key, val in vars.items()]
        accumulated += args
    cmds = [
        f"cp {os.path.join(self_dir, 'CACHEDIR.tag.in')} CACHEDIR.tag",
        f"cmake {source_dir} {' '.join(accumulated)}",
    ]

    print(" && ".join(cmds))
